//impordime sisse vahevara
const Express  = require ('express');

//impordime routerid
const mainRouter = require('./routes/main');

//defineerime serveri/rakenduse parameetrid
const hostname = '127.0.0.1';
const port =3000 ;

//paneme kokku rakenduse
const app = Express();

app.use('/', mainRouter);

//käivitame HTTP serveri
app.listen(port, hostname, () => {
    console.log('Server running at http://'+ hostname +':'+ port + '/');
    //console.log(`Server running at http://${hostname}:${port}/`);
});